package com.example.attendancesystem;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button mbtnlogin,mbtnlogout,mbtnattendance,mLeave,mRegistration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mbtnlogin= findViewById(R.id.checkin_button);
        mbtnlogout= findViewById(R.id.checkout_button);
        mbtnattendance=findViewById(R.id.attendancehistory);
        mLeave =findViewById(R.id.leavehistory);
        mRegistration = findViewById(R.id.employeregistration);

    }
}
